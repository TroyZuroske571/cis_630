
CIS630, Spring 2017, Term Project I, Due date: May 3, 2017

Please complete the following information, save this file and submit it along with your program

Your First and Last name: Troy Zuroske
Your Student ID: 951499420

What programming language did you use to write your code? C++

Does your program compile on ix (CIS department server)? Yes

How should we compile your program on ix? (please provide a makefile) 
In the applications folder there contains a Makefile, PageRank.cpp (source code), and this readme.txt file. From the terminal in application directory type: "make all" and this will create the executable. Now in order to run the program as an mpi application the command must be given exactly as so:

mpiexec -np Partitions ./PageRank GraphFile Partitionfile rounds 

Please note the number of partitions (processes) must be specified first which is different then the assignment's website because this is the only way to tell MPI how many partitions we want to run. So for example, if were run the program with two partitions to read the Flickr input files, perform 5 rounds and write the output of each round in the output files on ix we would run:

mpiexec -np 2 ./PageRank fl_compact.tab.txt fl_compact_part.2.txt 5

Does your program run on ix? Yes

Does your program calculate the credit values accurately? Yes

Does your program have a limit for the number of nodes it can handle in the input graph? No


How long does it take for your program with two partitions to read the Flickr input files, perform 5 round and write the output of each round in the output files on ix-dev?
The average time is 18-19 seconds. The fastest time is 16 seconds. However, in some cases on the first run after just uploading the files to ix the file input takes 9 seconds instead of 5-6 seconds. I am not sure why but please run the code at least one more time if this happens as you will see the correct average execution time of 18-19 seconds. 

Does your program run correctly with four partitions on ix-dev? Yes

Does you program end gracefully after completing the specified number of rounds? Yes
