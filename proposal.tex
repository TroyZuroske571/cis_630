\documentclass[12pt,twocolumn]{IEEEtran11}

\usepackage{times}
\usepackage{epsfig}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{subfigure}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\oddsidemargin -15pt
\evensidemargin -15pt
\leftmargin 0 pt
\topmargin -30pt
\textwidth = 6.9 in
\textheight = 9.0 in

\newcommand{\itembase}{\setlength{\itemsep}{0pt}}
\newcommand{\eg}{{\it e.g., }}
\newcommand{\ie}{{\it i.e., }}
\graphicspath{{FIG/}}

\begin{document}
\bibliographystyle{IEEE}

\title{\Large \bf A Survey of Techniques for Designing and Implementing Distributed Storage Systems
%\thanks{
}
\author{
Troy Zuroske\\
Information and Computer Science Department\\
University of Oregon\\
{\em trz@cs.uoregon.edu}\\
Student ID: 951499420\\
CIS 630 Spring 2017\\
}
\maketitle
% You have to do this to suppress page numbers.  Don't ask.
%\pagestyle{empty}

\section{Motivation and Theme}
\label{sec:intro}
%\input{intro}
	The amount of data that businesses, organizations, and people are creating is growing everyday. In fact, "Every day, we create 2.5 quintillion bytes of data - so much that 90\% of the data in the world today has been created in the last two years alone." \cite{MarrForbes:BigData}. With this emerging amount of data we need to be able to store and access this data as efficiently as possible. We have reached a point at which the traditional approach to storage of using a stand-alone, specialized storage box is no longer sufficient, for both technical and economical reasons. The best solution available to us now for storing data is distributed storage. In short, distributed storage is the ability to store data on a multitude of standard servers, which behave as one storage system although data is distributed between these servers. Distributed storage systems are preferred over traditional centralized file systems because of fault tolerance, availability, scalability and performance.\par
	The scope of this paper is comparing and contrasting a number of well known techniques for distributed storage management with respect to their efficiency (and possibly other measures). Efficiency could mean a number of items but in this case we will use the definition demonstrated in the paper, "Measurements of a Distributed File System"\cite{Baker:Measurements}. Efficiency is measured by file throughput, number of errors that occur involving cache consistency and other factors, and scalability. Scalability is not mentioned in the previous paper but it needs to be considered for this survey because if we are going to compare a system such as Google's Bigtable system to another paper's system which uses significantly less data to evaluate performance, the smaller system needs to be able scale accordingly to adequately be compared to Google's system.\par

\section{Paper Selection}
\label{sec:papers}
	We began our search for papers using Google Scholar. We started by searching broadly with "Distributed Storage Systems" and after reading over a few abstracts the first paper we looked in detail at was "Bigtable: A Distributed Storage System for Structured Data"\cite{Chang:2006:BDS:1267308.1267323}. This paper was written by a number of Google developers and has been citied 333 times. This paper described an approach to designing and implementing a cutting edge distributed storage system and then described the system's performance evaluation. This is then when we came up with the theme of this survey paper. We wanted to know other efficient approaches that exist for distributed storage systems so we decided to find seven other diverse techniques to compare and contrast in an attempt to find which one is most efficient.\par
	Google is a leading tech company that thrives because it is state of the art so we decided to research what other major tech companies with vast amounts of data use to implement their distributed storage system. This would make comparing and contrasting significantly more realistic since the companies are competitors and would be evaluating their systems using large amounts of data as well. The first competitor found was Amazon. Amazon produced a paper titled, "Dynamo: Amazon's Highly Available Key-value Store"\cite{DeCandia:2007:DAH:1294261.1294281}. The company states that Dynamo is their solution for managing their core services that they use to provide an "always-on" experience. Bigtable is actually discussed (minimally) in this paper.\par
	Another technique mentioned in Amazon's Dynamo paper is Antiquity. "Antiquity is a wide-area distributed storage system designed to provide a simple storage service for applications like file systems and back-up."\cite{Weatherspoon:2007:AES:1272996.1273035} Dynamo states that it is different for a number of reasons, one being that "Dynamo does not focus on the problem of data integrity and security and is built for a trusted environment."\cite{DeCandia:2007:DAH:1294261.1294281} This statement made "Antiquity: Exploiting a Secure Log for Wide-area Distributed Storage" a perfect candidate for our third technique to be surveyed.\par 
	Next to appear on the Google Scholar's search was the social network giant, Facebook. Their system serves hundreds of millions users at peak times. Facebook implemented a distributed storage system called Cassandra\cite{Lakshman:2010:CDS:1773912.1773922}. Bigtable and Dynamo are also both discussed in this paper making Cassandra another great distributed system technique for this survey. However, this paper was much shorter (six pages) than all of our other papers being presented so we found another eight page paper discussing Casandra and presents an implementation of their architecture providing results from an evaluation using the Yahoo Cloud Serving Benchmark (YCSB) on the Amazon EC2 Cloud\cite{6753779}. With these two papers we will be able to give a detailed survey of Casandra.\par
	We then looked at a slightly older paper but produced by another major tech leader, IBM. This paper is called "Performance of the IBM General Parallel File System"\cite{846052}. The paper includes structure and function of General Parallel File System but is focused primarily on performance evaluation. This will be our fifth system to be evaluated.\par 
	Next we wanted to look at another popular technique for distributed storage systems which is a peer-to-peer file-sharing system. Casandra has some peer-to-peer like attributes but we want a purely base system using the peer-to-peer technique. This technique is much different than the others proposed and has many applications implementing it. We decided to look at the most popular system in terms of number of users, called BitTorrent. BitTorrent has millions of users and its performance was surveyed in detail by Raymond Lei Xia and Jogesh K. Muppala in "A Survey of BitTorrent Performance"\cite{Xia:2010:SBP:2211317.2211361}. We will use these measurement results to compare to our other systems.\par
	At this point we were out of ideas for new types of distributed storage systems so we did some research and found another called BlobSeer. BlobSeer substitutes the "original Hadoop File System layer of Hadoop with a new, concurrency - optimized data storage layer based on the BlobSeer data management service"\cite{5470433}. The paper cited evaluates BlobSeer to the original Hadoop system but we will use its evaluation to compare to our other systems. Microsoft is optimizing data processing on Microsoft's Azure clouds based on BlobSeer data management system so the system is being used in industry.\par
	Our last approach we selected is not as famous as our other techniques as it is currently not being used by major tech companies but we found it to be relatively different than the others as it proposes a solution to improving tree-like structured data centers which three of the other techniques are using in some form. It also provides a performance evaluation of their proposal. This technique is "A Universal Distributed Indexing Scheme for Data Centers with Tree-Like Topologies"\cite{Liu:2015:UDI:2977646.2977657}. The paper proposes "U2-Tree, a universal distributed secondary indexing scheme built on cloud storage systems with tree-like topologies." This will be our final system that we survey. All papers will relied on equally and we do not anticipate to replace any of the selected papers later on. \par
	
\section{Key Issues}
\label{sec:issues}
	We plan to organize this paper by presenting each paper that provides a different technique for implementing a distributed file system. Presenting each paper's approach will show the similarities and differences between the techniques used. We will begin with Google's Bigtable that resembles database implementation strategies but "does not support a full relational data model; instead, it provides clients with a simple data model that supports dynamic control over data layout and format, and allows clients to reason about the locality properties of the data represented in the underlying storage"\cite{Chang:2006:BDS:1267308.1267323}. Following this paper we will present Amazon's Dynamo system. "Compared to Bigtable, Dynamo targets applications that require only key/value access with primary focus on high availability where updates are not rejected even in the wake of network partitions or server failures."\cite{DeCandia:2007:DAH:1294261.1294281} We will continue this pattern of presenting all approaches proposed by each paper comparing the techniques specifically on how data is stored, accessed, availability, scalability, how each handles faults (fault tolerant), limitations such as read only, hardware/software requirements (resource cost), and any other major specific issues or features a technique may present.\par
	Our selection of techniques was based on trying to find the best approach from each class of distributed file systems so we have a diverse set while also attempting to find systems that are widely used in practice. We believe seven out of the eight approaches we will present demonstrate the best distributed file system technique from that class of systems because leading tech companies developed, perfected, and implemented them for their company's use. Our final technique ("A Universal Distributed Indexing Scheme for Data Centers with Tree-Like Topologies") is a new concept that was recently released that can actually be applied as a general approach for secondary index on data centers with tree-like topologies. Tree-like structures were discussed in three of the other papers so we thought this would be an informative addition on another way they could be implemented and improved for efficiency. The specific comparison topics (addressed in the previous paragraph) we selected to discuss to compare each system were chosen because they are the constant themes discussed in every system's paper. How a system stores data is important because there are a number of ways to do this and each approach has advantages and disadvantages. How data is stored directly effects how it is accessed and effects how fast the system can provide information to the requesting client which is vital in a distributed system. Scalability is a core aspect of distributed storage systems because as data increases everyday the system must be able to scale to handle this. A fault tolerant is a necessary attribute all systems must have but these techniques handle failures differently. For example Dynamo sacrifices consistency under certain failure scenarios to make sure they can maintain their always on attribute. Limitations of a system is crucial to its evaluation because if a system is read only this directly effects its usefulness for users that may need more than this even if it is the most efficient read only system. Similar to this issue is resource cost, if a system requires expensive hardware to run this must be addressed in the survey paper because another system may be slightly less efficient but cost significantly less to run and maintain. We plan to evaluate each system by presenting how each one implements or addresses these essential evaluation topics to create a successful distributed storage system thus allowing for consistent comparing and contrasting between the techniques.\par
	Our final results section of our paper will be the performance evaluation of each system compared to one another. This section will contain primarily quantitative data such as file throughput and number of errors that occur. This is different than our fault tolerant evaluation topic because in that previous section we will only discuss how each system handles errors not how often they occur. We wanted to include these measurement results in its own section because by our definition of efficiency this a crucial part of our evaluation of a system and an opportunity to perhaps see major differences in numbers. Our hope is to make graphic with each system comparing file throughput and other notations of speed (indexing, reading, errors). This will complete our survey paper and should provide a very detailed insight into the most widely used techniques of distributed storage systems.\par
 
%% file citations.bib contains all the biblography
\bibliography{citations}
\end{document}












