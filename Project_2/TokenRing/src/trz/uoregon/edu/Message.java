package trz.uoregon.edu;

import java.io.Serializable;

public class Message implements Serializable 
{
	public Message(double id, int port, String msg, Boolean bProbe) 
	{
		this.id = id;
		this.port = port;
		this.msg = msg;
		this.bProbe = bProbe; 
	}

	private static final long serialVersionUID = 1L;
	private double id;
	private int port;
	private String msg;
	private Boolean bProbe; 

	public double getId() 
	{
		return id;
	}

	public void setId(double id)
	{
		this.id = id;
	}

	public int getPort() 
	{
		return port;
	}

	public void setPort(int port) 
	{
		this.port = port;
	}

	public String getMsg() 
	{
		return msg;
	}

	public void setMsg(String msg) 
	{
		this.msg = msg;
	}
	
	public Boolean getProbe() 
	{
		return bProbe;
	}

	public void setProbe(Boolean bProbe) 
	{
		this.bProbe = bProbe;
	}
}


