package trz.uoregon.edu;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.AbstractMap;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of Token Client CIS 630. 
 * 
 *
 * @author  Troy Zuroske, University of Oregon
 */

public class client
{
	static String name;
	static int sourcePort;
	static String destinationIP = "127.0.0.1"; 
	static int destinationPort; 
	static int receivingPort = 0;
	static Channel channel = new Channel();
	static double currentMessageId; 
	static int startPort = 0; 
	static int endPort = 0;
	static long joinTime; 
	static long leaveTime; 
	static boolean bRingLeader = false; 
	static Queue<Entry<Long,String>> allMessages = new LinkedList<Entry<Long,String>>();
	static Stack<Integer> portsInRing = new Stack<Integer>(); 
	static Queue<Entry<Long,String>> messagesToPost = new LinkedList<Entry<Long,String>>(); 
	static boolean bProbeFound = false; 
	static boolean portFound = false; 
	static boolean bRebuildRing = false; 
	static double currentProbeId = ThreadLocalRandom.current().nextInt(0, 10000 + 1);
	static double currentElectionId = ThreadLocalRandom.current().nextInt(0, 10000 + 1);
	static InetSocketAddress address = null;
	static int ringLeaderPort = 0; 
	static Boolean isLeader = false; 
	static boolean allMessagesReceived = true; 
	static String outputFilePath; 
	static boolean token = false; 
	static double currentTokenId = ThreadLocalRandom.current().nextInt(0, 10000 + 1);
	static boolean sendToken = true; 
	static long roundTripTimeMsg = 2000; 
	static long tokenTimeOut = 0; 
	static long startingTime = System.currentTimeMillis(); 
	static Writer writer = null;
	static boolean outputToken = true;
	static boolean leaderElected = false; 

	public static void main(String[] args) throws IOException 
	{	
		int configFile = 0; 
		int inputFile = 0;
		int outputFileIdx = 0; 
		
		if (args.length != 6)
		{
			System.err.println("Input error, command should be: java trz/uoregon/edu/client "
					+ "(-c config#.txt file) (-i input#.txt file) (-o out#.txt");
			System.exit(-1); 
		}
		else
		{
			for (int i = 0; i < args.length; i++)
			{
				if (args[i].equals("-c"))
				{
					configFile = i + 1;
				}
				if (args[i].equals("-i"))
				{
					inputFile = i + 1;
				}
				if(args[i].equals("-o"))
				{
					outputFileIdx = i + 1; 
				}
			}
			
			BufferedReader reader = new BufferedReader(new FileReader(args[configFile]));
			String line;
			while ((line = reader.readLine()) != null)
			{
				String[] tokens = line.split(" ");
				if(tokens[0].equals("client_port:"))
				{
					String[] portRange = tokens[1].split("-"); 
			    	startPort = Integer.parseInt(portRange[0]);
			    	endPort = Integer.parseInt(portRange[1]);
				}
				else if (tokens[0].equals("my_port:"))
				{
			    	sourcePort = Integer.parseInt(tokens[1]); 
			    	ringLeaderPort = sourcePort;
				}
				else if (tokens[0].equals("join_time:"))
				{
			    	joinTime = convertToMilSec(tokens[1]); 
				}
				else if (tokens[0].equals("leave_time:"))
				{
			    	leaveTime = convertToMilSec(tokens[1]);
			    	leaveTime += System.currentTimeMillis(); 
				}
			}
			reader.close();		
			
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(args[outputFileIdx]), "utf-8")); 
			
			reader = new BufferedReader(new FileReader(args[inputFile]));
			String newline;
			while ((newline = reader.readLine()) != null)
			{
				String[] tokens = newline.split("\\t");	
				long convertTime = convertToMilSec(tokens[0]); 
				allMessages.add(new AbstractMap.SimpleEntry<Long, String>(convertTime, tokens[1])); 				 
			}
			
		}
		long join = new Date().getTime();
		while(new Date().getTime() - join < joinTime){}
		System.out.println("Node joining.");
		channel.bind(sourcePort);
		channel.start();
		portFound = rebuildRing();
		bRebuildRing = false; 
		if (portFound)
		{	
			System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": ring is broken"); 
			System.out.flush();
			writer.write(getTime(System.currentTimeMillis() - startingTime) + ": ring is broken");
			((BufferedWriter) writer).newLine();
			address = new InetSocketAddress(destinationIP, destinationPort);
			int attempts = 0;
			try {
				double randElectTimer = Math.random();
				Thread.sleep((long) randElectTimer);
				isLeader = false;
				while (leaderElected == false)
				{
					startElection(destinationPort);
					long start = new Date().getTime();
					while(new Date().getTime() - start < 2000L){}
					attempts++; 
					if (attempts > 5)
					{
						System.out.println(attempts);
						bRebuildRing = true;
						break; 
					}
				}
				if (isLeader == true)
				{
					startToken(destinationPort);
				}
				System.out.println(bRebuildRing);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (attempts < 5)
			{
				bRebuildRing = false; 
			}
			attempts = 0; 
		}
		else
		{
			System.out.println("All ports scanned.");
		}

		while (System.currentTimeMillis() < leaveTime)
		{
			try {
				Thread.sleep(500);
			} catch (InterruptedException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			if (bRebuildRing)
			{
				token = false; 
				System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": ring is broken"); 
				System.out.flush();
				writer.write(getTime(System.currentTimeMillis() - startingTime) + ": ring is broken");
				((BufferedWriter) writer).newLine();
				portFound = rebuildRing(); 
				int attempts = 0;
				if (portFound)
				{
					address = new InetSocketAddress(destinationIP, destinationPort);
					try {
						double randElectTimer = Math.random();
						Thread.sleep((long) randElectTimer);
						isLeader = false;
						while (leaderElected == false)
						{
							startElection(destinationPort);
							long start = new Date().getTime();
							while(new Date().getTime() - start < 2000L){}
							attempts++; 
							if (attempts > 5)
							{
								bRebuildRing = true;
								break; 
							}
						}
						if (isLeader == true)
						{
							startToken(destinationPort);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (attempts < 5)
				{
					bRebuildRing = false; 
				}
				attempts = 0; 
			}
			while (portFound && (bRebuildRing == false) && System.currentTimeMillis() < leaveTime && leaderElected == true)
			{
				if (bRebuildRing == true)
				{
					break; 
				}
				long temp = System.currentTimeMillis() - startingTime; 
				if (allMessages.isEmpty() == false)
				{
					while (temp > allMessages.peek().getKey())
					{	
						messagesToPost.add(allMessages.poll()); 
						if (allMessages.isEmpty() == true)
						{
							break; 
						}
					}			
				}				
			
				sendToken = true; 
				try {
					Thread.sleep(500);
				} catch (InterruptedException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				if (token == true)
				{
					if (bRebuildRing == true)
					{
						break; 
					}
					if (roundTripTimeMsg != 0)
					{
						roundTripTimeMsg = ((System.currentTimeMillis() - tokenTimeOut) * 2); 
					}
					while (messagesToPost.isEmpty() == false)
					{
						if (allMessagesReceived == true)
						{
							if (bRebuildRing == true)
							{
								break; 
							}
							currentMessageId = ThreadLocalRandom.current().nextInt(0, 10000 + 1);
							Message message = new Message(currentMessageId, sourcePort, messagesToPost.peek().getValue(), false); 
							
							try {
								if (bRebuildRing == true)
								{
									break; 
								}
								allMessagesReceived = false; 
								System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": post " + "\"" +message.getMsg() +"\"" + " was sent");
								writer.write(getTime(System.currentTimeMillis() - startingTime) + ": post " + "\"" +message.getMsg() +"\"" + " was sent");
								((BufferedWriter) writer).newLine();
								channel.sendTo(address, message);
								long tempRTT = 0;
								while (allMessagesReceived == false && tempRTT < 2000)
								{
									Thread.sleep(500);
									if (allMessagesReceived == true)
									{
										break; 
									}
									else
									{
										tempRTT += 500; 
									}
								}
								//Thread.sleep(roundTripTime);
								if (allMessagesReceived == true)
								{
									tokenTimeOut = System.currentTimeMillis();
									System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": post " + "\"" + message.getMsg() + "\"" + 
											" was delivered to all successfully"); 
									writer.write(getTime(System.currentTimeMillis() - startingTime) + ": post " + "\"" + message.getMsg() + "\"" + 
											" was delivered to all successfully");
									((BufferedWriter) writer).newLine();
									if (tempRTT == 0)
									{
										tempRTT = 100; 
									}
									roundTripTimeMsg = (tempRTT * 2); 
									messagesToPost.poll(); 
								}
								else
								{
									System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": ring is broken"); 
									System.out.flush();
									writer.write(getTime(System.currentTimeMillis() - startingTime) + ": ring is broken");
									((BufferedWriter) writer).newLine();
									receivingPort = 0;
									destinationPort = 0; 
									portFound = rebuildRing();
									if (portFound == true)
									{
										bRebuildRing = false; 
										address = new InetSocketAddress(destinationIP, destinationPort);
										allMessagesReceived = true; 
										roundTripTimeMsg = 2000; 
										bRebuildRing = false; 
										double randElectTimer = Math.random();
										try {
											Thread.sleep((long) randElectTimer);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										leaderElected = false; 
										isLeader = false;
										bRebuildRing = false; 
										outputToken = true;
										int attempts = 0;
										while (leaderElected == false)
										{
											startElection(destinationPort);
											long start = new Date().getTime();
											while(new Date().getTime() - start < 2000L){}
											attempts++; 
											if (attempts > 5)
											{
												bRebuildRing = true;
												break; 
											}
										}
										if (isLeader == true)
										{
											bRebuildRing = false; 
											startToken(destinationPort);
										}
										if (attempts < 5)
										{
											bRebuildRing = false; 
										}
										attempts = 0; 
										tokenTimeOut = System.currentTimeMillis();
									}
									break; 
								}
							} catch (IOException | InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
					if (sendToken == true)
					{
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						InetSocketAddress address = new InetSocketAddress(destinationIP, destinationPort);
						Message tokenMsg = new Message (currentTokenId, destinationPort, "token", false); 
						while(bRebuildRing == false)
						{
							channel.sendTo(address, tokenMsg);
							tokenTimeOut = System.currentTimeMillis();
							if (outputToken == true)
							{
								System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": token " + currentTokenId + " was sent to client " + destinationPort);
								try {
									writer.write(getTime(System.currentTimeMillis() - startingTime) + ": token " + currentTokenId + " was sent to client" + destinationPort);
									((BufferedWriter) writer).newLine();
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} 
								outputToken = false; 
							}
							token = false; 
							break; 
						}
					}
				}
				else if ((System.currentTimeMillis() - tokenTimeOut) > 4000L && token == false && bRebuildRing == false && leaderElected == true)
				{
					System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": ring is broken");
					System.out.flush();
					writer.write(getTime(System.currentTimeMillis() - startingTime) + ": ring is broken");
					((BufferedWriter) writer).newLine();
					receivingPort = 0;
					destinationPort = 0; 
					portFound = rebuildRing();
					if (portFound == true)
					{
						bRebuildRing = false; 
						address = new InetSocketAddress(destinationIP, destinationPort);
						allMessagesReceived = true; 
						roundTripTimeMsg = 2000; 
						bRebuildRing = false; 
						double randElectTimer = Math.random();
						try {
							Thread.sleep((long) randElectTimer);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						leaderElected = false; 
						isLeader = false;
						bRebuildRing = false; 
						outputToken = true;
						int attempts = 0;
						while (leaderElected == false)
						{
							startElection(destinationPort);
							long start = new Date().getTime();
							while(new Date().getTime() - start < 2000L){}
							attempts++; 
							if (attempts > 5)
							{
								bRebuildRing = true;
								break; 
							}
						}
						if (isLeader == true)
						{
							bRebuildRing = false; 
							startToken(destinationPort);
						}
						if (attempts < 5)
						{
							bRebuildRing = false; 
						}
						attempts = 0; 
						tokenTimeOut = System.currentTimeMillis();
					}
					break; 
				}
			}
				/*else 
				{
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (token == true)
					{
						InetSocketAddress address = new InetSocketAddress(destinationIP, destinationPort);
						Message tokenMsg = new Message (currentTokenId, destinationPort, "token", false); 
						while(bRebuildRing == false)
						{
							channel.sendTo(address, tokenMsg);
							tokenTimeOut = System.currentTimeMillis();
							//System.out.println("token " + currentTokenId + " was sent to client " + destinationPort); 
							token = false; 
							break; 
						}
					}
					else if ((System.currentTimeMillis() - tokenTimeOut) > 2000 && token == false)
					{
						System.out.println(token);
						System.out.println("Token lost and not received in time allowed.");
						portFound = rebuildRing();
						address = new InetSocketAddress(destinationIP, destinationPort);
						allMessagesReceived = true; 
						tokenTimeOut = System.currentTimeMillis();
						roundTripTimeMsg = 2000; 
						double randElectTimer = Math.random();
						try {
							Thread.sleep((long) randElectTimer);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Election started.");
						startElection(destinationPort);
					}
				}*/ 
				
				
					/*else if (messagesToPost.isEmpty() == false && token == true)
					{
						while (messagesToPost.isEmpty() == false)
						{
							System.out.println("I have the token." );
							if (allMessagesReceived == true)
							{
								currentMessageId = UUID.randomUUID(); 
								Message message = new Message(currentMessageId, sourcePort, msg, false); 
								
								try {
									System.out.println(message.getMsg());
									allMessagesReceived = false; 
									channel.sendTo(address, message);
									Thread.sleep(2000);
								} catch (IOException | InterruptedException e) {
									e.printStackTrace();
								}
							}
						}
						token = false; 
					}*/
		}
		writer.close();
		System.out.println("Node leaving.");
		System.exit(0); 
	}
	
	public static String getTime(long millis)
	{
		String time = String.format("%02d:%02d", 
			    TimeUnit.MILLISECONDS.toMinutes(millis),
			    TimeUnit.MILLISECONDS.toSeconds(millis) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
			);
		return time; 
	}
	
	public static long convertToMilSec(String time)
	{
		String[] units = time.split(":"); 
		int minutes = Integer.parseInt(units[0]); 
		int seconds = Integer.parseInt(units[1]); 
		long duration = (60000 * minutes) + (1000 * seconds); 
		return duration; 
	}
	
	private static void startToken(int port) 
	{			
        InetSocketAddress tempAddress = new InetSocketAddress(destinationIP, port);	
		
		String msg = "token";
		currentTokenId = ThreadLocalRandom.current().nextInt(0, 10000 + 1); 
		System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": new token generated " + currentTokenId);
		try {
			writer.write(getTime(System.currentTimeMillis() - startingTime) + ": new token generated " + currentTokenId);
			((BufferedWriter) writer).newLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Message tempMessage = new Message(currentTokenId, sourcePort, msg, false); 
		try {
			channel.sendTo(tempAddress, tempMessage);
			tokenTimeOut = System.currentTimeMillis();
			System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": token " + currentTokenId + " was sent to client " + port);
			try {
				writer.write(getTime(System.currentTimeMillis() - startingTime) + ": token " + currentTokenId + " was sent to client " + port);
				((BufferedWriter) writer).newLine();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			outputToken = false; 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	private static void startElection(int port) 
	{			
        InetSocketAddress tempAddress = new InetSocketAddress(destinationIP, port);	
		leaderElected = false; 
		String msg = "election";	 
		currentElectionId = ThreadLocalRandom.current().nextInt(0, 10000 + 1);
		Message tempMessage = new Message(currentElectionId, sourcePort, msg, false); 
		try {
			System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": started election, send election message to client " + port);
			writer.write(getTime(System.currentTimeMillis() - startingTime) + ": started election, send election message to client " + port);
			((BufferedWriter) writer).newLine();
			channel.sendTo(tempAddress, tempMessage);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}      
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		/*while(isLeader == null)
		{
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (isLeader)
		{
			startToken(destinationPort);
		}*/
	}
	
	private static boolean bPortInUse(int port) 
	{			
        InetSocketAddress tempAddress = new InetSocketAddress(destinationIP, port);	
		
		String msg = "probe";
		Message tempMessage = new Message(currentProbeId, sourcePort, msg, false); 
		try {
			channel.sendTo(tempAddress, tempMessage);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        if (bProbeFound == false)
	        {
	        	return false;
	        }
	        else
	        {
	        	return true; 
	        }   
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false; 
		}  
	}
	
	private static boolean rebuildRing()
	{
		token = false; 
		bRebuildRing = true; 
		portsInRing.clear();
		currentProbeId = ThreadLocalRandom.current().nextInt(0, 10000 + 1);
		//Begin port scanning
		for (int i = sourcePort + 1; i <= endPort; i++)
		{
			if (bPortInUse(i))
			{
				portsInRing.push(i); 
				bProbeFound = false; 
				break;
			}
		}
		if (portsInRing.size() == 0)
		{
			bRingLeader = true; 
			for (int j = startPort; j < sourcePort; j++)
			{
				if (bPortInUse(j))
				{
					portsInRing.push(j); 
					bProbeFound = false; 
					break; 
				}
			}
			if (portsInRing.size() != 0)
			{
				int previous = destinationPort;
				destinationPort = portsInRing.get(0); 
				if (previous != destinationPort)
				{
					System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": next hop is changed to client " + destinationPort);
					String temp = getTime(System.currentTimeMillis() - startingTime) + ": next hop is changed to client " + destinationPort; 
					try {
						writer.write(temp);
						((BufferedWriter) writer).newLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				return true; 
			}
			else
			{
				return false; 
			}
		}
		else
		{
			int previous = destinationPort;
			destinationPort = portsInRing.get(0); 
			if (previous != destinationPort)
			{
			
				System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": next hop is changed to client " + destinationPort);
				String temp = getTime(System.currentTimeMillis() - startingTime) + ": next hop is changed to client " + destinationPort; 
				try {
					writer.write(temp);
					((BufferedWriter) writer).newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return true; 
		}
	}
	
	public static class Channel implements Runnable
	{
		private DatagramSocket socket;
		private boolean running;
		
		public void bind(int port) throws SocketException
		{
			socket = new DatagramSocket(port);
		}
		
		public void start()
		{
			Thread thread = new Thread(this);
			thread.start();
		}
		
		public void stop()
		{
			running = false;
			socket.close();
		}

		@Override
		public void run()
		{
			byte[] buffer = new byte[1024];
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			
			running = true;
			while(running)
			{				
				try
				{
					socket.receive(packet);
					byte[] data = packet.getData();
					ByteArrayInputStream in = new ByteArrayInputStream(data);
					ObjectInputStream is = new ObjectInputStream(in);
					Message msg = (Message) is.readObject();
					String tempStr = "probe";
					String tempStrElection = "election"; 
					String tempStrToken = "token"; 
					if (msg.getMsg().equals(tempStr))
					{
						System.out.println("RECEIVED PROBE FROM: " + packet.getPort()); 
						tokenTimeOut = System.currentTimeMillis();
						if (msg.getProbe() == false)
						{
							InetSocketAddress address = new InetSocketAddress(destinationIP, msg.getPort());
							msg.setProbe(true);
							channel.sendTo(address, msg);
							double tempProbeId = msg.getId();
							if (currentProbeId == tempProbeId)
							{	
								bRebuildRing = false; 	
								System.out.println(bRebuildRing);
							}
							else
							{
								bRebuildRing = true;
								currentProbeId = msg.getId(); 
								int tempPort = msg.getPort(); 
								if (tempPort != receivingPort)
								{
									bRebuildRing = true;
									receivingPort = msg.getPort(); 
									System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": previous hop is changed to client " + receivingPort); 
									writer.write(getTime(System.currentTimeMillis() - startingTime) + ": previous hop is changed to client " + receivingPort);
									((BufferedWriter) writer).newLine();
									bRebuildRing = true;
								}
								bRebuildRing = true;
							}
						}
						else
						{
							System.out.println("Final else statement.");
							bProbeFound = true; 
							bRebuildRing = false;
						}
					}
					else if (msg.getMsg().equals(tempStrElection))
					{
						tokenTimeOut = System.currentTimeMillis();
						InetSocketAddress address = new InetSocketAddress(destinationIP, destinationPort);
						int tempPort = msg.getPort();
						if (receivingPort == 0)
						{
							receivingPort = packet.getPort(); 
						}
						if (receivingPort == packet.getPort())
						{
							if (tempPort >= ringLeaderPort && (currentElectionId != msg.getId()))
							{
								ringLeaderPort = tempPort;
								isLeader = false; 
								
								channel.sendTo(address, msg);	
								System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": relayed election message, leader: client " + ringLeaderPort);
								writer.write(getTime(System.currentTimeMillis() - startingTime) + ": relayed election message, leader: client " + ringLeaderPort);
								((BufferedWriter) writer).newLine();
							}
							else if (tempPort < ringLeaderPort && (currentElectionId != msg.getId()))
							{
								ringLeaderPort = sourcePort;
								
								channel.sendTo(address, msg);
								System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": relayed election message, replaced leader " + ringLeaderPort);
								writer.write(getTime(System.currentTimeMillis() - startingTime) + ": relayed election message, replaced leader " + ringLeaderPort);						
								((BufferedWriter) writer).newLine();
							}
							else if (currentElectionId == msg.getId())
							{
								if (msg.getPort() == ringLeaderPort && destinationPort < ringLeaderPort)
								{
									isLeader = true; 
									System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": leader selected");
									writer.write(getTime(System.currentTimeMillis() - startingTime) + ": leader selected");						
									((BufferedWriter) writer).newLine();
									//startToken(destinationPort);
								}
								else
								{
									isLeader = false;
								}
								leaderElected = true; 
							}
						}
					}
					else if (msg.getMsg().equals(tempStrToken))
					{	
						tokenTimeOut = System.currentTimeMillis();
						token = true; 
						if (receivingPort == 0)
						{
							receivingPort = packet.getPort(); 
						}
						if (receivingPort == packet.getPort())
						{
							if (currentTokenId != msg.getId())
							{	
								currentTokenId = msg.getId();
								if (outputToken == true)
								{
									System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": token " + currentTokenId + " was received");
									writer.write(getTime(System.currentTimeMillis() - startingTime) + ": token " + currentTokenId + " was received");										
									((BufferedWriter) writer).newLine();
									outputToken = false;
								}
							}
							else if (isLeader == true)
							{
								if (outputToken == true)
								{
									System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": token " + currentTokenId + " was received");
									writer.write(getTime(System.currentTimeMillis() - startingTime) + ": token " + currentTokenId + " was received");										
									((BufferedWriter) writer).newLine();
									outputToken = false;
								}
							}
						}
					}
					else
					{
						while(bRebuildRing == false)
						{
							tokenTimeOut = System.currentTimeMillis();
							displayAndFoward(msg);
							break;
						}
					}
					
				} 
				catch (IOException e)
				{
				} catch (ClassNotFoundException e) {
				}		
			}
		}
		
		public void displayAndFoward(Message msg)
		{
			try 
			{
				double tempId = msg.getId(); 
				if (tempId != currentMessageId)
				{
					receivingPort = msg.getPort(); 
					InetSocketAddress address = new InetSocketAddress(destinationIP, destinationPort);
					channel.sendTo(address, msg);
					System.out.println(getTime(System.currentTimeMillis() - startingTime) + ": post " + "\"" + msg.getMsg() + "\"" + " from client " +
					msg.getPort() + " was relayed");
					writer.write(getTime(System.currentTimeMillis() - startingTime) + ": post " + "\"" + msg.getMsg() + "\"" + " from client " +
					msg.getPort() + " was relayed");
					((BufferedWriter) writer).newLine();
				}
				else
				{
					receivingPort = msg.getPort(); 
					allMessagesReceived = true; 
				}
		
			} catch (IOException e) 
			{
				e.printStackTrace();
			}			
		}
		
		public void sendTo(SocketAddress address, Message message) throws IOException
		{
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ObjectOutputStream os = new ObjectOutputStream(outputStream);
			os.writeObject(message);
			byte[] buffer = outputStream.toByteArray();
			
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			packet.setSocketAddress(address);
			
			socket.send(packet);
		}		
	 }	
  }
	

