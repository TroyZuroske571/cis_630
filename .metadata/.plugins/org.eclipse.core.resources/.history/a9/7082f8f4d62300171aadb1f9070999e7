/*
 ============================================================================
 Name        : RandomWalk.cpp
 Author      : Troy Zuroske
 Version     : 1.0
 Description : Project 1 for CIS 630
 ============================================================================
 */

#include <iostream>
#include <time.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <cstdio>
#include <mpi.h>
#include <string>

using namespace std;

//Globals
const int DEGREE = 0;
const int PARTITION = 1;
FILE *file_Input, *file_Output;
char hostname[MPI_MAX_PROCESSOR_NAME];
int numRounds, rank, numTasks, len;
unsigned int largestNode = 0;
unsigned int** aryPartitions;
unsigned int** aryNeighbors;

/******************************************************************************/
// Method:    getTime
//
// Purpose:   Get the difference in time between start and stop timers
//
// Arguments: startTime - The time when the timer starter
//
// Returned:  The difference in time.
/******************************************************************************/
double getTime(time_t startTime)
{
    time_t stopTime;
    time(&stopTime);
    return difftime(stopTime, startTime);
}

/******************************************************************************/
// Method:    parsePartitionFile
//
// Purpose:   Parse partition file store array of nodes, degrees, partition ids.
//
// Arguments: partitionFile - Pointer to the file to be parsed.
//
// Returned:  None.
/******************************************************************************/
void parsePartitionFile(const char* partitionFile)
{
	unsigned int nodeID, nodeDegree, partitionID;

	// Read in partition file to find the max node
	file_Input = fopen(partitionFile, "r");
	if (file_Input != NULL)
	{
		// get largest node in file
		while (fscanf(file_Input, "%u %u %u", &nodeID, &nodeDegree, &partitionID) == 3)
		{
			if (nodeID > largestNode)
			{
				largestNode = nodeID;
			}
		}

		// create space for partition and neighbor arrays
		aryPartitions = (unsigned int**) malloc(largestNode *
									sizeof(unsigned int*));
		aryNeighbors = (unsigned int**) malloc(largestNode *
									sizeof(unsigned int*));

		for (unsigned int i = 0; i < largestNode; i++)
		{
			aryPartitions[i] = (unsigned int*) malloc(2 *
							sizeof(unsigned int*));
		}

		//reset the file pointer to the beginning of the file
		fseek(file_Input, 0, SEEK_SET);
		while (fscanf(file_Input, "%u %u %u", &nodeID, &nodeDegree, &partitionID) == 3)
		{
			aryPartitions[nodeID-1][DEGREE] = nodeDegree;
			aryPartitions[nodeID-1][PARTITION] = partitionID;
			aryNeighbors[nodeID-1] = (unsigned int*)malloc(nodeDegree * sizeof(unsigned int));
		}
		fclose(file_Input);
	}
	else
	{
		cerr << "Error! Can't open partition input file '" << partitionFile << "'" << endl;
		exit(EXIT_FAILURE);
	}
}

void parseGraphFile(const char* graphFile)
{
	int* indexAryNeigh = (int*) malloc(largestNode * sizeof(int));
	unsigned int neightbor1, neighbor2;
	file_Input = fopen(graphFile, "r");
    if (file_Input != NULL)
    {
    	while (fscanf(file_Input, "%u %u", &neightbor1, &neighbor2) == 2)
		{
    		// set neighbor 2 to neighbor of 1 and vice versa
			aryNeighbors[neightbor1-1][indexAryNeigh[neightbor1-1]] = neighbor2-1;
			indexAryNeigh[neightbor1-1] += 1;
			aryNeighbors[neighbor2-1][indexAryNeigh[neighbor2-1]] = neightbor1-1;
			indexAryNeigh[neighbor2-1] += 1;
		}
		fclose(file_Input);
    }
    else
	{
		cerr << "Error opening graph file: " << graphFile << endl;
		exit(EXIT_FAILURE);
	}

	free(indexAryNeigh);
}

int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		cerr << "Incorrect syntax. Program requires: prog GraphFile Partitionfile rounds Partitions." << endl;
		exit(EXIT_FAILURE);
	}

	numRounds = atoi(argv[3]);
	if (numRounds <= 0)
	{
		cerr << "Number of rounds must be at least 1. You entered: " << argv[3] << endl;
		exit(EXIT_FAILURE);
	}
	char* temp;
	strcpy(temp, argv[4]);
	char* graph;
	strcpy(graph, argv[1]);
	char* part;
	strcpy(part, argv[2]);
	strcpy(argv[1],"-np");
	strcpy(argv[2], temp);

	// initialize MPI
	MPI_Init(&argc, &argv);

	// get number of tasks
	MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

	// get my rank
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// get host name
	MPI_Get_processor_name(hostname, &len);
	printf ("Number of tasks= %d My rank= %d Running on %s\n", numTasks,rank,hostname);

	// Calculate time it takes to read in input files
	time_t inputTimer;
	time(&inputTimer);


	parsePartitionFile(part);

	parseGraphFile(graph);

	cout << "time to read input files, partition " << rank << " = " << getTime(inputTimer) << "sec" << endl;


	// done with MPI
	MPI_Finalize();
    return EXIT_SUCCESS;
}

