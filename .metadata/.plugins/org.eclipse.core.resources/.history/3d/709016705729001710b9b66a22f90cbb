/*==============================================================================

 Author: Jordan Weiler
 Date:   June 2, 2014

 This MPI program simulates the behavior of many random walks on a large graph.
 It takes an input file listing node partitions and node neighbors (degrees),
 an input file listing edges in the undirected graph, and a command line
 parameter for the number of rounds to simulate. An output file for each MPI
 process will be created and will contain the nodes within the process, the
 number of degrees for each node, and each node's credit for each round in the
 simulation. The output file name will be in the form of #.out, where # is the
 rank of the MPI process.

 Each MPI process will calculate the credit for the nodes contained within its
 partition. For nodes outside of its partition, the MPI process will rely on
 MPI communication to receive the credits of external nodes. Each node starts
 with a credit of 1.0 before simultation begins. For each subsequent round of
 simulation, the node's credit is determined by the credit of the each neighbor
 of the node divided by each neighbor's degree.

 For example, if node i has four neighbors with credit values of 0.1, 0.23,
 0.35, and 0.64 and node degrees of 5, 27, 19, and 8 respectively, the credit
 for node i after the round would be:

 c(t+1, i) = (0.1/5) + (0.23/27) + (0.35/19) + (0.64/8)
           = 0.126940

==============================================================================*/

#include <iostream>
#include <time.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <cstdio>
#include <mpi.h>
#include <string>
#include <vector>
#include <algorithm>
#include <fcntl.h>

using namespace std;

// *****************************************************************************
// Global variables
// *****************************************************************************
int numberOfRounds, processRank, maxTasks;
unsigned int** aryPartitions;
unsigned int** aryNeighbors;
unsigned int largestNode = 0;
FILE *fileInput, *fileOutput;
size_t i, j, k;
string strOutputFile;
double** aryCreds;
const int DEGREE = 0;
const int PARTITION = 1;
const float BEG_CREDS = 1.0;


// *****************************************************************************
// Method: getDiffTime
//
// Purpose:
//   Calculate the time difference between the start and stop timers
//
// Arguments:
//   start - the start timer
//
// *****************************************************************************
double getDiffTime(time_t start)
{
    time_t stop;
    time(&stop);
    return difftime(stop, start);
}

// *****************************************************************************
// Method: readEdgeFile
//
// Purpose:
//   Read in the edge input file and populate the array of neighbors for each
//   node
//
// Arguments:
//   edgeFile - name for the edge input file
//
// *****************************************************************************
void parseGraphFile(const char* graphFile)
{
	int* indexAryNeigh = (int*) malloc(largestNode * sizeof(int));
	unsigned int neightbor1, neighbor2;
	fileInput = fopen(graphFile, "r");
    if (fileInput != NULL)
    {
    	while (fscanf(fileInput, "%u %u", &neightbor1, &neighbor2) == 2)
		{
    		// set neighbor 2 to neighbor of 1 and vice versa
			aryNeighbors[neightbor1-1][indexAryNeigh[neightbor1-1]] = neighbor2-1;
			indexAryNeigh[neightbor1-1] += 1;
			aryNeighbors[neighbor2-1][indexAryNeigh[neighbor2-1]] = neightbor1-1;
			indexAryNeigh[neighbor2-1] += 1;
		}
		fclose(fileInput);
    }
    else
	{
		cerr << "Error opening graph file: " << graphFile << endl;
		exit(EXIT_FAILURE);
	}

	free(indexAryNeigh);
}

// *****************************************************************************
// Method: readPartitionFile
//
// Purpose:
//   Read in partition file. Create array of nodes, degrees, and partition ids.
//
// Arguments:
//   partitionFile - name for the input partition file
//
// *****************************************************************************
void parsePartitionFile(const char* partitionFile)
{
    unsigned int nodeID, nodeDegree, partitionID;

    // Read in partition file to find the max node
    fileInput = fopen(partitionFile, "r");
    int fd = fileno(fileInput);
    posix_fadvise(fd, 0, 0, 1);
    fseek(fileInput, 0L, SEEK_END);
    long int sz = ftell(fileInput);
    unsigned int* tempStor = (unsigned int*) malloc(sz * sizeof(unsigned int*));
    unsigned int tempStorSize = 0;
    rewind(fileInput);
    i = 0;
    if (fileInput != NULL)
    {
        while (fscanf(fileInput, "%u %u %u", &nodeID, &nodeDegree, &partitionID) == 3)
        {
            if (nodeID > largestNode)
            {
                largestNode = nodeID;
            }
            tempStor[i] = nodeID;
            tempStorSize++;
            i++;
            tempStor[i] = nodeDegree;
            tempStorSize++;
            i++;
            tempStor[i] = partitionID;
            tempStorSize++;
            i++;
        }
        // Create arrays of max node size for partitions and neighbors
		aryPartitions = (unsigned int**) malloc(largestNode *
							sizeof(unsigned int*));
		aryNeighbors = (unsigned int**) malloc(largestNode *
							sizeof(unsigned int*));

		for (i = 0; i < largestNode; i++)
		{
			aryPartitions[i] = (unsigned int*) malloc(2 * sizeof(unsigned int*));
		}

		for (i = 0; i < tempStorSize; i+=3)
		{
			unsigned int tempID = tempStor[i];
			aryPartitions[tempID-1][DEGREE] = tempStor[i + 1];
			aryPartitions[tempID-1][PARTITION] = tempStor[i + 2];
			aryNeighbors[tempID-1] = (unsigned int*)malloc(tempStor[i + 1]
							* sizeof(unsigned int));
		}


		free(tempStor);
    }
    else
    {
        cerr << "Error! Can't open partition input file '" << partitionFile << "'" << endl;
        exit(EXIT_FAILURE);
    }

    fclose(fileInput);
}

// *****************************************************************************
// Method: simulateRandomWalk
//
// Purpose:
//   Simulate the act of running several random walks. This will loop through
//   each node and calculate the node's credit based off of their neighbor's
//   credits divided by their neighbor's degree. Each node's credit will get
//   updated to the node's line in the output file during the looping phase.
//   Each process will write out the nodes (and credit) in its partition only.
//
// *****************************************************************************
void calculatePageRank()
{
	MPI_Status status;
	time_t startTime;
	size_t sizeOfLine = 4096;
	size_t length = 0;
	int previousIndex = 0;
	int nextIndex = 1;
	double creditCalculated;
	unsigned int currentIndex = 0;


	// Create temporary output file name based on process rank
	ostringstream oss;
	oss << processRank;
	string tempFileName = string("tempFile.") + oss.str();

	// Create send and receive buffers for round credit MPI communication
	double *sendMsg = (double*) malloc(largestNode * sizeof(double));
	double *getMsg = (double*) malloc(largestNode * sizeof(double));

	// Simulate the random walk for the given number of rounds
	for (int round = 0; round < numberOfRounds; round++)
	{
		// Start each round at the same time across all processes
		MPI_Barrier(MPI_COMM_WORLD);
		time(&startTime);

		// Assign the send buffer to the local credits calculated last round
		for (i = 0; i < largestNode; i++)
		{
			if (aryPartitions[i][PARTITION] == (unsigned)processRank)
			{
				sendMsg[i] = aryCreds[previousIndex][i];
			}
		}

		// Loop through all partition to partition combinations. Use MPI to
		// send and receive the previously calculated credits of last round so
		// that each partition can have the entire list of credits locally and
		// avoid sending other MPI communication to get any external credits
		// at a later time.
		for (int sendRank = 0; sendRank < maxTasks; sendRank++)
		{
			for (int recvRank = 0; recvRank < maxTasks; recvRank++)
			{
				if (sendRank != recvRank)
				{
					if (sendRank == processRank)
					{
						// Send local credit to a different partition
						MPI_Send(sendMsg, largestNode, MPI_DOUBLE, recvRank, 5, MPI_COMM_WORLD);
					}
					else if (recvRank == processRank)
					{
						// Receive credit from a different partition
						MPI_Recv(getMsg, largestNode, MPI_DOUBLE, sendRank, 5, MPI_COMM_WORLD, &status);
						for (k = 0; k < largestNode; k++)
						{
							// If the partition id of the node matches the
							// send rank, that credit in the buffer needs to
							// get copied to the credit array for last round.
							if (aryPartitions[k][PARTITION] == (unsigned)sendRank)
							{
								aryCreds[previousIndex][k] = getMsg[k];
							}
						}

					}
				}
			}
		}

		// Let all processes catch up from the MPI communication
		MPI_Barrier(MPI_COMM_WORLD);

		//** NOTE **
		//** We now have all credits across all processes and can simulate **

		char line_current [sizeOfLine];

		// Open output file to read previous credits line
		fileInput = fopen(strOutputFile.c_str(), "r");
		if (fileInput == NULL)
		{
			cerr << "Error! Can't open output file '" << strOutputFile << "'" << endl;
			exit(EXIT_FAILURE);
		}

		// Open temp output file to write new credits
		fileOutput = fopen(tempFileName.c_str(), "w");
		if (fileOutput == NULL)
		{
			cerr << "Error! Can't open temp output file '" << tempFileName << "'" << endl;
			exit(EXIT_FAILURE);
		}

		unsigned int node = 0;

		// Spin through each file line, append the new credit to the node's
		// line and write back out to temp file
		while (fgets(line_current, sizeOfLine, fileInput) != NULL)
		{
			if (node >= largestNode)
			{
				cerr << "Error! Counter is past size of nodes" << endl;
				cerr << "node=" << node << " max node size=" << largestNode << endl;
				exit(EXIT_FAILURE);
			}

			// Increment i until the current node is in the process's partition
			// and the node has neighbors
			while ((aryPartitions[node][PARTITION] != (unsigned)processRank
					or aryPartitions[node][DEGREE] <= 0) && node < largestNode)
			{
				node++;
			}

			// Credit for a node is equal to each neighbor's credit divided by
			// each neighbor's degree size
			creditCalculated = 0;
			for (j = 0; j < aryPartitions[node][DEGREE]; j++)
			{
				currentIndex = aryNeighbors[node][j];
				creditCalculated += aryCreds[previousIndex][currentIndex] /
									aryPartitions[currentIndex][DEGREE];
			}
			aryCreds[nextIndex][i] = creditCalculated;

			// Strip off line break from end of line
			length = strlen(line_current);
			if (line_current[length-1] == '\n')
			{
				line_current[length-1] = 0;
			}

			// Write out previous file line then tab then node's credit for
			// current round
			fprintf(fileOutput, "%s\t%f\n", line_current, creditCalculated);

			node++;
		}
		fclose(fileInput);
		fclose(fileOutput);

		// Double the size for the file line if the current line length is
		// within 100 characters of the end of the file line
		if (length > sizeOfLine - 100)
		{
			sizeOfLine *= 2;
		}

		// Remove last round's output file
		remove(strOutputFile.c_str());

		// Rename this round's temporary file to given output file name
		rename(tempFileName.c_str(), strOutputFile.c_str());

		// Toggle credit indices
		previousIndex = nextIndex;
		nextIndex = (nextIndex + 1) % 2;

		// Print out time for round
		cout << " ---- time for round " << (round + 1) << ", partition " << processRank << " = " << getDiffTime(startTime) << "sec" << endl;

		// First barrier for MPI processes to get to this point
		MPI_Barrier(MPI_COMM_WORLD);

		// Second barrier for std output to get written out
		MPI_Barrier(MPI_COMM_WORLD);

		// Print out the overall time for all partitions for that round of
		// processing
		if (processRank == 0)
		{
			cout << "total time for round " << (round + 1) << ": " << getDiffTime(startTime) << "sec" << endl;
		}
	}

	// Make sure all processes delete their buffers at the same time
	MPI_Barrier(MPI_COMM_WORLD);

	free(sendMsg);
	free(getMsg);
}


// *****************************************************************************
// Method: validateInputArguments
//
// Purpose:
//   Validates the input arguments against necessary requirements
//
// Arguments:
//   argc - number of command line arguments
//   argv - char array containing each argument
//
// *****************************************************************************
void validateInputArguments(int argc, char* argv[])
{
    if (argc < 4)
    {
        cerr << "Error! Correct syntax is: prog Nodes2partition EdgeView numRounds" << endl;
        exit(EXIT_FAILURE);
    }
    
    numberOfRounds = atoi(argv[3]);
    if (numberOfRounds <= 0)
    {
        cerr << "Error! Number of rounds must be > 0, not " << argv[3] << endl;
        exit(EXIT_FAILURE);
    }
}

// *****************************************************************************
// Method: main
//
// Purpose:
//   Main function called when program is invoked
//
// Arguments:
//   argc - number of command line arguments
//   argv - char array containing each argument
//
// *****************************************************************************
int main(int argc, char* argv[])
{
    // Start up MPI
    MPI_Init(&argc, &argv);

    // extract current processes rank into the rank variable
    MPI_Comm_rank(MPI_COMM_WORLD, &processRank);
    
    // extract the total number of processes into the number_of_tasks variable
    MPI_Comm_size(MPI_COMM_WORLD, &maxTasks);

    validateInputArguments(argc, argv);

    // Calculate time it takes to read in input files
    time_t readFileTimer;
    time(&readFileTimer);
    
    parsePartitionFile(argv[1]);

    parseGraphFile(argv[2]);

    cout << "time to read input files, partition " << processRank << " = " << getDiffTime(readFileTimer) << "sec" << endl;
    
    //Create output files to write to
	MPI_Barrier(MPI_COMM_WORLD);

	ostringstream osstream;
	osstream << processRank;
	strOutputFile = osstream.str() + string(".out");

	aryCreds = (double**) malloc(2 * sizeof(double*));

	for (int i = 0; i < 2; i++)
	{
		aryCreds[i] = (double*) malloc(largestNode * sizeof(double*));
	}

	int n = 0;
	fileOutput = fopen(strOutputFile.c_str(), "w");
	if (fileOutput != NULL)
	{
		for (long unsigned int i = 0; i < largestNode; i++)
		{
			// Only write out node if it is part of the current process's
			// partition and if it has neighbors (i.e. found in input file)
			if (aryPartitions[i][PARTITION] == (unsigned)processRank &&
					aryPartitions[i][DEGREE] > 0)
			{
				// Initialize credit of node to 1
				aryCreds[0][i] = 1.0;

				// Write out long unsigned node, then tab, then long unsigned
				// neighbor size
				char buffer [50];
				n = sprintf (buffer, "%lu\t%d\n", (i+1),
						aryPartitions[i][DEGREE]);
				fwrite (buffer, sizeof(char), n, fileOutput);
			}
		}
		fclose(fileOutput);
	}
	else
	{
		cerr << "Error! Can't open output file '" << strOutputFile << "'" << endl;
		exit(EXIT_FAILURE);
	}
    calculatePageRank();
    
    for (int i = 0; i < 2; i++)
	{
		free(aryCreds[i]);
	}
	free(aryCreds);

	for (unsigned int i = 0; i < largestNode; i++)
	{
		if (aryPartitions[i][DEGREE] > 0)
		{
			free(aryNeighbors[i]);
		}
		free(aryPartitions[i]);
	}
	free(aryNeighbors);
	free(aryPartitions);


    MPI_Finalize();
    return EXIT_SUCCESS;
}
