/*
 ===============================================================================
 Name        : PageRank.cpp
 Author      : Troy Zuroske
 Version     : 1.0
 Description : Project 1 for CIS 630
 ===============================================================================
 */

#include <mpi.h>
#include <string>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <time.h>
#include <sstream>
#include <cstdio>

using namespace std;

//function prototypes
double getDiffTime (time_t);
unsigned int parsePartitionFile(const char*, unsigned int***, unsigned int***);
void parseGraphFile(const char*, unsigned int***, unsigned int);
time_t getPreviousCredits(int, unsigned int,unsigned int ***, double ***);
void calculatePageRank(unsigned int, int, unsigned int***, unsigned int***,
		double***);

/*******************************************************************************
 Function:    main

 Description: Initiates program execution, creates temporary output file.

 Parameters:  argv[1] - Graph file path
 	 	 	  argv[2] - Partition file path
 	 	 	  argv[3] - Number of rounds

 Returned:    Success if no error, else -1
*******************************************************************************/
int main(int argc, char* argv[])
{
	int numberOfRounds, processRank, maxTasks;
	unsigned int** aryPartitions;
	unsigned int** aryNeighbors;
	unsigned int largestNode = 0;
	string strOutputFile;
	FILE *fileOutput;
	double** aryCreds = (double**) malloc(2 * sizeof(double*));;

	if (argc < 4)
	{
		cerr << "Incorrect syntax. Must be: GraphFile Partition file rounds."
				<< endl;
		exit(EXIT_FAILURE);
	}

	numberOfRounds = atoi(argv[3]);
	if (numberOfRounds <= 0)
	{
		cerr << "Invalid number of rounds, must be more than zero. "<< endl;
		exit(EXIT_FAILURE);
	}

    // Start up MPI
	MPI_Init(&argc, &argv);

	// extract current processes rank into the rank variable
	MPI_Comm_rank(MPI_COMM_WORLD, &processRank);

	// extract the total number of processes into the number_of_tasks variable
	MPI_Comm_size(MPI_COMM_WORLD, &maxTasks);

	// Calculate time it takes to read in input files
	time_t readFileTimer;
	time(&readFileTimer);
	largestNode = parsePartitionFile(argv[2], &aryPartitions, &aryNeighbors);
	parseGraphFile(argv[1], &aryNeighbors, largestNode);

	MPI_Barrier(MPI_COMM_WORLD);

	cout << "time to read input files, partition " << processRank << " = "
		 << getDiffTime(readFileTimer) << "sec" << endl;
	fflush(stdout);


	ostringstream osstream;
	osstream << processRank;
	strOutputFile = osstream.str() + string(".out");

	for (int i = 0; i < 2; i++)
	{
		aryCreds[i] = (double*) malloc(largestNode * sizeof(double*));
	}

	int n = 0;
	fileOutput = fopen(strOutputFile.c_str(), "w");
	if (fileOutput != NULL)
	{
		for (long unsigned int i = 0; i < largestNode; i++)
		{
			if (aryPartitions[i][1] == (unsigned)processRank &&
					aryPartitions[i][0] > 0)
			{
				aryCreds[0][i] = 1.0;

				char buffer [50];
				n = sprintf (buffer, "%lu\t%d\n", (i+1),
						aryPartitions[i][0]);
				fwrite (buffer, sizeof(char), n, fileOutput);
			}
		}
		fclose(fileOutput);
	}
	else
	{
		cerr << "Error opening file: " << strOutputFile << endl;
		exit(EXIT_FAILURE);
	}

	MPI_Barrier(MPI_COMM_WORLD);

    calculatePageRank(largestNode, numberOfRounds, &aryPartitions,
    		          &aryNeighbors, &aryCreds);

    for (int i = 0; i < 2; i++)
	{
		free(aryCreds[i]);
	}
	free(aryCreds);

	for (unsigned int i = 0; i < largestNode; i++)
	{
		if (aryPartitions[i][0] > 0)
		{
			free(aryNeighbors[i]);
		}
		free(aryPartitions[i]);
	}
	free(aryNeighbors);
	free(aryPartitions);


    MPI_Finalize();
    return EXIT_SUCCESS;
}

/*******************************************************************************
 Function:    getDiffTime

 Description: Get the total time for an action such as reading in files or
  	  	  	  compeleting a round.

 Parameters:  start - Starting time of the timer

 Returned:    The difference between start time and end time in seconds
*******************************************************************************/
double getDiffTime(time_t start)
{
    time_t stop;
    time(&stop);
    return difftime(stop, start);
}

/*******************************************************************************
 Function:    parseGraphFile

 Description: Get the input graph file and set up neighbors in corresponding
 	 	 	  arrays.

 Parameters:  graphFile    - Character array with path to graph file
 	 	 	  aryNeighbors - Pointer to double array for storing node neighbors
 	 	 	  largestNode  - Unsigned integer with the largest in the file

 Returned:    None
*******************************************************************************/
void parseGraphFile(const char* graphFile, unsigned int ***aryNeighbors,
							unsigned int largestNode)
{
	int* indexAryNeigh = (int*) malloc(largestNode * sizeof(int));
	FILE * fileInput = fopen(graphFile, "r");
	int fd = fileno(fileInput);
	posix_fadvise(fd, 0, 0, 1);
	fseek(fileInput, 0L, SEEK_END);
	long int sz = ftell(fileInput);
	unsigned int* tempStor = (unsigned int*) malloc(sz * sizeof(unsigned int*));
	char buffer[50];
	size_t tempStorSize = 0;
	rewind(fileInput);

    if (fileInput != NULL)
    {
    	while (fgets(buffer, sizeof(buffer), fileInput))
		{
			char *next = buffer;
			while(*next && *next != '\n')
			{
				tempStor[tempStorSize++] = strtol(next, &next, 0);
			}
			unsigned int nOne = tempStor[tempStorSize-2]-1;
			unsigned int nTwo = tempStor[tempStorSize-1]-1;
			(*aryNeighbors)[nOne][indexAryNeigh[nOne]] = nTwo;
			indexAryNeigh[nOne] += 1;
			(*aryNeighbors)[nTwo][indexAryNeigh[nTwo]] = nOne;
			indexAryNeigh[nTwo] += 1;
		}
		fclose(fileInput);
    }
    else
	{
		cerr << "Error opening graph file: " << graphFile << endl;
		exit(EXIT_FAILURE);
	}

    free(tempStor);
	free(indexAryNeigh);
}

/*******************************************************************************
 Function:    parsePartitionFile

 Description: Get the input partition file, set up array memory, and assign
  	  	  	  degree and partition.

 Parameters:  partitionFile - Character array with path to graph file
 	 	 	  aryPartitions - Pointer to double array for storing which node is
 	 	 	  	  	  	  	  assigned to each partition.
 	 	 	  aryNeighbors  - Pointer to double array for storing node neighbors

 Returned:    largestNode   - Unsigned integer with the largest in the file
*******************************************************************************/
unsigned int parsePartitionFile(const char* partitionFile,
					unsigned int ***aryPartitions, unsigned int ***aryNeighbors)
{
    FILE * fileInput = fopen(partitionFile, "r");
    int fd = fileno(fileInput);
    posix_fadvise(fd, 0, 0, 1);
    fseek(fileInput, 0L, SEEK_END);
    long int sz = ftell(fileInput);
    unsigned int* tempStor = (unsigned int*) malloc(sz * sizeof(unsigned int*));
    char buffer[50];
    size_t tempStorSize = 0;
    rewind(fileInput);
    unsigned int i = 0;
    unsigned int largestNode;

    if (fileInput != NULL)
    {
		while (fgets(buffer, sizeof(buffer), fileInput))
		{
			char *next = buffer;
			while(*next && *next != '\n')
			{
				tempStor[tempStorSize++] = strtol(next, &next, 0);
			}

			if (tempStor[tempStorSize-3] > largestNode)
			{
				largestNode = tempStor[tempStorSize-3];
			}
		}

		//allocate memory
		*aryPartitions = (unsigned int**) malloc(largestNode *
				                                 sizeof(unsigned int*));
		*aryNeighbors = (unsigned int**) malloc(largestNode *
				                                sizeof(unsigned int*));

		for (i = 0; i < largestNode; i++)
		{
			(*aryPartitions)[i] = (unsigned int*) malloc(2 *
					                                     sizeof(unsigned int*));
		}

		for (i = 0; i < tempStorSize; i+=3)
		{
			unsigned int tempID = tempStor[i];
			(*aryPartitions)[tempID-1][0] = tempStor[i + 1];
			(*aryPartitions)[tempID-1][1] = tempStor[i + 2];
			(*aryNeighbors)[tempID-1] = (unsigned int*)malloc(tempStor[i + 1]
							* sizeof(unsigned int));
		}

		free(tempStor);
    }
    else
    {
        cerr << "Error opening partition input file." << endl;
        exit(EXIT_FAILURE);
    }

    fclose(fileInput);
    return largestNode;
}

/*******************************************************************************
 Function:    getPreviousCredits

 Description: Get and receive the last calculated credits of the prior round so
			  each process has all of the credits needed for calculation.

 Parameters:  previousIndex - The previous spot used in the aryCreds array.
 	 	 	  largestNode   - Unsigned integer with the largest in the file
 	 	 	  aryPartitions - Pointer to double array for storing which node is
 	 	 	  	  	  	  	  assigned to each partition.
 	 	 	  aryCreds      - Pointer to double that has/stores local credits

 Returned:    startTime     - The time we started this round of calculation.
*******************************************************************************/
time_t getPreviousCredits(int previousIndex, unsigned int largestNode,
						  unsigned int ***aryPartitions, double ***aryCreds)
{
	MPI_Status status;
	int processRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &processRank);
	int maxTasks;
	MPI_Comm_size(MPI_COMM_WORLD, &maxTasks);
	time_t startTime;
	double *sendMsg = (double*) malloc(largestNode * sizeof(double));
	double *getMsg = (double*) malloc(largestNode * sizeof(double));
	size_t i;
	time(&startTime);

	for (i = 0; i < largestNode; i++)
	{
		if ((*aryPartitions)[i][1] == (unsigned)processRank)
		{
			sendMsg[i] = (*aryCreds)[previousIndex][i];
		}
	}

	for (int recvIndex = 0; recvIndex < maxTasks; recvIndex++)
	{
		for (int sendIndex = 0; sendIndex < maxTasks; sendIndex++)
		{
			if (recvIndex != sendIndex)
			{
				if (recvIndex == processRank)
				{
					MPI_Recv(getMsg, largestNode, MPI_DOUBLE, sendIndex, 0,
												MPI_COMM_WORLD, &status);
					for (unsigned int z = 0; z < largestNode; z++)
					{
						if ((*aryPartitions)[z][1] == (unsigned)sendIndex)
						{
							(*aryCreds)[previousIndex][z] = getMsg[z];
						}
					}
				}
				else if (sendIndex == processRank)
				{
					MPI_Send(sendMsg, largestNode, MPI_DOUBLE, recvIndex, 0,
							MPI_COMM_WORLD);
				}
			}
		}
	}
	free(sendMsg);
	free(getMsg);
	return startTime;
}

/*******************************************************************************
 Function:    calculatePageRank

 Description: This function calculates the PageRank on the provided graph. We
 	 	 	  loop through each node on the graph and calculate the rank based
 	 	 	  on the provided formula provided here:
 	 	 	  https://classes.cs.uoregon.edu/17S/cis630/LectureNotes/CodeCogsEqn.gif
 	 	 	  As the number of rounds increases, the credit of a node converges
 	 	 	  to its PageRank value. We write the value of each node's credit
 	 	 	  after every round to the processRank.out file.

 Parameters:  largestNode    - Unsigned integer with the largest in the file
 	 	 	  numberOfRounds - Integer that holds the number of rounds we are
 	 	 	  	  	  	  	   running the page rank through.
 	 	 	  aryPartitions  - Pointer to double array for storing which node is
 	 	 	  	  	  	  	   assigned to each partition.
 	 	 	  aryNeighbors   - Pointer to double array for storing node neighbor
 	 	 	  aryCreds       - Pointer to double that has/stores local credits

 Returned:    None
*******************************************************************************/
void calculatePageRank(unsigned int largestNode, int numberOfRounds,
		               unsigned int ***aryPartitions,
					   unsigned int ***aryNeighbors, double ***aryCreds)
{
	int processRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &processRank);
	time_t startTime;
	int previousIndex = 0;
	int nextIndex = 1;
	double creditCalculated;
	unsigned int currentIndex = 0;
	FILE *fileInput, *fileOutput;
	size_t currentLine = 0;
	size_t maxLineSize = 1024;
	string strOutputFile;

	ostringstream osstream;
	osstream << processRank;
	strOutputFile = osstream.str() + string(".out");
	ostringstream tempOsstream;
	tempOsstream << processRank;
	string tempFileName = string("tempFile.") + tempOsstream.str();

	for (int round = 0; round < numberOfRounds; round++)
	{
		MPI_Barrier(MPI_COMM_WORLD);

		startTime = getPreviousCredits(previousIndex, largestNode,
				                       aryPartitions, aryCreds);

		MPI_Barrier(MPI_COMM_WORLD);

		fileOutput = fopen(tempFileName.c_str(), "w");
		fileInput = fopen(strOutputFile.c_str(), "r");
		unsigned int node = 0;
		char buffer[maxLineSize];

		while (fgets(buffer, maxLineSize, fileInput) != NULL)
		{
			while (((*aryPartitions)[node][1] != (unsigned)processRank
					or (*aryPartitions)[node][0] <= 0) && node < largestNode)
			{
				node++;
			}

			creditCalculated = 0;
			for (unsigned int m = 0; m < (*aryPartitions)[node][0]; m++)
			{
				currentIndex = (*aryNeighbors)[node][m];
				creditCalculated += (*aryCreds)[previousIndex][currentIndex] /
						(*aryPartitions)[currentIndex][0];
			}
			(*aryCreds)[nextIndex][node] = creditCalculated;

			currentLine = strlen(buffer);
			if (buffer[currentLine-1] == '\n')
			{
				buffer[currentLine-1] = 0;
			}
			char buf[maxLineSize];
			sprintf(buf, "%s\t%f\n", buffer, creditCalculated);
			fwrite(buf, 1, strlen(buf), fileOutput);
			node++;
		}
		fclose(fileInput);
		fclose(fileOutput);

		remove(strOutputFile.c_str());
		rename(tempFileName.c_str(), strOutputFile.c_str());

		previousIndex = nextIndex;
		nextIndex = (nextIndex + 1) % 2;

		cout << " --- time for round " << (round + 1) << ", partition " <<
				processRank << " = " << getDiffTime(startTime) << "sec" << endl
				<< flush;

		fflush(stdout);

		MPI_Barrier(MPI_COMM_WORLD);

		if (processRank == 1)
		{
			cout << "total time for round " << (round + 1) << ": " <<
					getDiffTime(startTime) << "sec" << endl << flush;
			fflush(stdout);
		}
		fflush(stdout);
		MPI_Barrier(MPI_COMM_WORLD);
	}

	MPI_Barrier(MPI_COMM_WORLD);
}
