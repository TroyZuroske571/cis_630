/*
 ============================================================================
 Name        : RandomWalk.cpp
 Author      : Troy Zuroske
 Version     : 1.0
 Description : Project 1 for CIS 630
 ============================================================================
 */

#include <iostream>
#include <time.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <cstdio>
#include <mpi.h>
#include <string>

using namespace std;

//Globals
const int DEGREE = 0;
const int PARTITION = 1;
FILE *file_Input, *file_Output;
char hostname[MPI_MAX_PROCESSOR_NAME];
int numRounds, rank, numTasks, len;
unsigned int largestNode = 0;
unsigned int** partitionArray;
unsigned int** neighborArray;

/******************************************************************************/
// Method:    getTime
//
// Purpose:   Get the difference in time between start and stop timers
//
// Arguments: startTime - The time when the timer starter
//
// Returned:  The difference in time.
/******************************************************************************/
double getTime(time_t startTime)
{
    time_t stopTime;
    time(&stopTime);
    return difftime(stopTime, startTime);
}

/******************************************************************************/
// Method:    parsePartitionFile
//
// Purpose:   Parse partition file store array of nodes, degrees, partition ids.
//
// Arguments: partitionFile - Pointer to the file to be parsed.
//
// Returned:  None.
/******************************************************************************/
void parsePartitionFile(const char* partitionFile)
{
	unsigned int nodeID, nodeDegree, partitionID;

	file_Input = fopen(partitionFile, "r");
	// get the largest node so we can use arrays instead of vectors for storage
	if (file_Input != NULL)
	{
		while (fscanf(file_Input, "%u %u %u", &nodeID, &nodeDegree, &partitionID) == 3)
		{
			if (nodeID > largestNode)
			{
				largestNode = nodeID;
			}
		}
		partitionArray = (unsigned int**) malloc(largestNode * sizeof(unsigned int));
		neighborArray = (unsigned int**) malloc(largestNode * sizeof(unsigned int));
		for (unsigned int i = 0; i < largestNode; i++)
		{
			partitionArray[i] = new unsigned int[2];
			partitionArray[i] = (unsigned int*) malloc(2 * sizeof(unsigned int));

		}

		while (fscanf(file_Input, "%u %u %u", &nodeID, &nodeDegree,
				&partitionID) == 3)
		{
			partitionArray[nodeID-1][DEGREE] = nodeDegree;
			partitionArray[nodeID-1][PARTITION] = partitionID;
			neighborArray[nodeID-1] = (unsigned int*)malloc(nodeDegree * sizeof(nodeDegree));
		}
	}
	else
	{
		cerr << "Error opening partition partitionFile: " << partitionFile << endl;
		exit(EXIT_FAILURE);
	}


}

int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		cerr << "Incorrect syntax. Program requires: prog GraphFile Partitionfile rounds Partitions." << endl;
		exit(EXIT_FAILURE);
	}

	numRounds = atoi(argv[3]);
	if (numRounds <= 0)
	{
		cerr << "Number of rounds must be at least 1. You entered: " << argv[3] << endl;
		exit(EXIT_FAILURE);
	}

	// initialize MPI
	//MPI_Init(&argc, &argv);

	// get number of tasks
	//MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

	// get my rank
	//MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// get host name
	//MPI_Get_processor_name(hostname, &len);
	//printf ("Number of tasks= %d My rank= %d Running on %s\n", numTasks,rank,hostname);

	// Calculate time it takes to read in input files
	time_t inputTimer;
	time(&inputTimer);


	parsePartitionFile(argv[2]);
	// done with MPI
	//MPI_Finalize();

    return EXIT_SUCCESS;
}

